#pragma once
#include <string>

namespace rdfcxx {
  class anon_node : node {
  public:
    anon_node(anonID id) : id{id} {}
     
  private:
    anonID id;
  };

  class anonID {
  public:
    anonID(std::string id) : id{id} {};

  private:
    std::string id;
  };
}
