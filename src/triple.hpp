#pragma once

namespace rdfcxx {
  class triple {
  private:
    node subject;
    node object;
    node predicate;
  };
};
