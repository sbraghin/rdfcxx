#pragma once

#include <string>

namespace rdfcxx {
  namespace graph {
    namespace nodefactory {
      node create_anon();
      node create_anon(anonID id);
      node create_literal(std::string value);
      node create_URI(std::string uri);
      node create_variable(std::string name);
    };

    class node {
    public:
      virtual bool isBlank() const noexcept = 0;
      virtual bool isConcrete() const noexcept = 0;
      virtual bool isURI() const noexcept = 0;
      virtual bool isLiteral() const noexcept = 0;
      virtual bool isVariable() const noexept = 0;
    };

    class uri_node : node {
    public:
      virtual bool isConcrete() const noexcept override { return true; };
      virtual bool isBlank() const noexcept override { return false;};
      virtual bool isURI() const noexcept override { return true;};
      virtual bool isVariable() const noexcept override { return false;};
      virtual bool isLiteral() const noexcept override { return false;};
    };

    class literal_node : node {
    public:
      virtual bool isConcrete() const noexcept override { return true; };
      virtual bool isBlank() const noexcept override { return false;};
      virtual bool isURI() const noexcept override { return false;};
      virtual bool isVariable() const noexcept override { return false;};
      virtual bool isLiteral() const noexcept override { return true;};
    }

    class blank_node : node {
    public:
      blank_node(anonID id) : id{id} {};

      virtual bool isConcrete() const noexcept override { return true; };
      virtual bool isBlank() const noexcept override { return true;};
      virtual bool isURI() const noexcept override { return false;};
      virtual bool isVariable() const noexcept override { return false;};
      virtual bool isLiteral() const noexcept override { return false;};

      anonID get_blank_id() const noexcept { return id; };

    private:
      anonID id;
    };

    class anonID {
    public:
      anonID(std::string id) : id{id} {};

    private:
      std::string id;
    };

    class triple {
    public:
      triple(node &subject, node &predicate, node &object);
    private:
      node subject, predicate, object;
    }

  }

}
