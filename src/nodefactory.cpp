#include "node.hpp"

static std::string create_random_id() {
  return "TEST";
};

node rdfcxx::nodefactory::create_blank() {
  return blank_node{anonID{create_random_id()}};
}

node rdfcxx::nodefactory::create_blank(anonID id) {
  return blank_node{id};
}

